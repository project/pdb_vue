// Create a vue instance that shares a global state via Pinia. Place this along
// with the "Vue3 Pinia A" component to see them interact.
Vue.createApp({
  template: `<p>Second Instance: {{count}}</p>`,
  computed: {
    ...Pinia.mapState(useCounterStore, ['count'])
  }
}).use(usePinia).mount('.vue3-pinia-b')
