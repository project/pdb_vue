// Create a vue3 instance that shares a global state via Pinia. Place this along
// with the "Vue3 Pinia B" component to see them interact.
Vue.createApp({
  template: `
    <div>
      <button @click="update">click me</button>
      <p>First Instance: {{count}}</p>
    </div>
  `,
  computed: {
    ...Pinia.mapState(useCounterStore, ['count'])
  },
  methods: {
    ...Pinia.mapActions(useCounterStore, ['increment']),

    update() {
      this.increment()
    }
  }
}).use(usePinia).mount('.vue3-pinia-a')
