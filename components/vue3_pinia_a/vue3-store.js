// This file could easily be defined as it's own library so that this component
// does not need to be a dependency of others.
const usePinia = Pinia.createPinia()

const useCounterStore = Pinia.defineStore('counter', {
  state() {
    return {
      count: 0
    }
  },
  actions: {
    increment() {
      this.count++
    }
  }
})
