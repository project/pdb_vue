// Create a vue3 component that can be reused.
vueApp.component('vue3-spa-component', {
  template: '<div class="test">{{ message }}</div>',
  props: {
    textField: {
      type: String
    },
    instanceId: {
      type: String
    }
  },
  data() {
    return {
      message: 'Hello Vue! This is a component so will work in multiple places.'
    };
  },
  mounted() {
    if (this.textField) {
      this.message = this.textField;
    }
  }
});
