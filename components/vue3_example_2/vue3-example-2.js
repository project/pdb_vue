// Create a root instance for each block.
const vueElements = document.querySelectorAll('.vue3-example-2');

// Loop through each block to create multiple instance on the page.
vueElements.forEach(element => {
  // Create a vue instance.
  Vue.createApp({
    data: () => ({
      message: 'Hello Vue3!'
    })
  }).mount(element)
});

