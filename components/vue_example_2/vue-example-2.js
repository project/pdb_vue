// Create a root instance for each block.
const vueElements = document.querySelectorAll('.vue-example-2');

// Loop through each block to create multiple instance on the page.
vueElements.forEach(element => {
  // Create a vue instance.
  new Vue({
    el: element,
    data: {
      message: 'Hello Vue!'
    }
  });
});
