// Mount the global Vue.js instance.
const appMount = (drupalSettings.pdbVue.spaElement) ? drupalSettings.pdbVue.spaElement : '#page-wrapper'
vueApp.mount(appMount)
