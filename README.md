# Decoupled Blocks: Vue.js

This is a [Vue.js](https://vuejs.org/) implementation for the
[Decoupled Blocks](https://www.drupal.org/project/pdb) module. Blocks built with
Vue.js can now easily be added to a site via a module or in a custom theme.

The [Decoupled Blocks](https://www.drupal.org/project/pdb) module is required
for this to work.

## Vue Version
Vue 3 is the default version, but a different version can be set on the
configuration page `/admin/config/services/pdb-vue`.

## Development Mode
Demo blocks can be enabled by turning on Development mode at
`/admin/config/services/pdb-vue`.

Development mode also uses the un-minified version of Vue so that
[Vue Devtools](https://github.com/vuejs/vue-devtools) can be used.

## Per-block Vue Instances

Vue.js is very flexible so several methods of creating vue blocks has been
implemented. By default it is assumed that each block is its own Vue instance.

### Basic Examples
`vue3_example_1` is the simplest example of using Vue to render in a single
block. The downside is that multiple blocks of the same type will not render.
Vue.js is different from jQuery in that it only renders the first instance it
finds and then stops. So even if a site editor adds multiples of the same block,
only the first one will render. This is fine if the block is really only
designed to be placed once per page.

```js
Vue.createApp({
  data() {
    return {
      message: 'Hello Vue! This will only work on a single Example 1 block per page.'
    }
  },
  template: `<div class="test">{{message}}</div>`
}).mount('.vue3-example-1')
```

### Using a separate template file
The `vue3_example_2` demo block uses a separate template file to place the
markup. This template is indicated in the `vue3_example_2.info.yml` file by
adding:

```yaml
template: template_name.html
```

### Basic Vue instance rendered multiple times
The `vue3_example_2` demo block also demonstrates how to make a block render
multiple times in the cases where it is possible that a site editor would place
more than one block of the same type. Truthfully, this is not very elegant.

With Javascript, first look for each block via a class name and then loop
through each one. Add a Vue instance within the loop that looks for the element
of the current instance of the loop.

```js
// Find all blocks that match the class name.
const vueElements = document.querySelectorAll('.vue3-example-2');
vueElements.forEach(element => {
  // Add a vue instance here passing "element" into .mount(element)
});
```

### Getting the instance ID
The ID of a block can be accessed so that configuration from the block may be
fetched. Get the block id within a Vue instance using `$el`.
```js
const instanceId = this.$el.parentElement.getAttribute('id');
```
With the Instance ID, raw settings for that block instance can now be accessed
from `drupalSettings.pdb.configuration[instanceId]`.

## Vue Components

Vue allows the use of [components](https://vuejs.org/v2/guide/components.html)
which are ideal for reuse. Using components requires a global Vue instance in
order to render all components.

### Enabling "Single Page App" mode
Go to `/admin/config/services/pdb-vue` and check the box to "Use Vue components
in a Single Page App format." This will then present a text field where you can
define the element which the vue instance will attach itself to. By default it
uses the `#page-wrapper` provided by the Classy theme.

By enabling this mode, a `vue3.spa-init.js` javascript file will be added after
all components, thus rendering them.

Finally, each component needs to specify that it is a Vue component in the
`*.info.yml` file.

```yaml
component: true
```

### App instance
Vue 3 requires that components be added to an app instance using `createApp()`.
This has already been done and assigned to a `vueApp` variable.

```js
// Already created in the vue3.spa-create.js file.
const vueApp = Vue.createApp({});
```

Use the `vueApp` variable to attach components.

```js
vueApp.component();
```

### Passing block configuration settings as component properties
Each block can be set up to allow a user to add settings which will be passed
into a component as [Props](https://vuejs.org/guide/components/props.html).

To add fields to the block configuration, add them to the `*.info.yml` file.
```yaml
configuration:
  fieldName:
    type: textfield
    default_value: 'I am a default value'
```
Then in the Vue component just add `props` for those fields.
```js
props: ['textField']
```

### Getting the instance ID
Each component is automatically passed an `instance-id` prop. This can be
accessed in the Vue instance by just adding the camelCase name into `props`
```js
props: ['textField', 'instanceId']
```
With the Instance ID, raw settings for that block instance can now be accessed
from `drupalSettings.pdb.configuration[this.instanceId]`.

### Slots

Currently the use of [Slots](https://vuejs.org/guide/components/slots.html)
in top level components is not possible. This means that when a block is added
and configured, that configuration will only pass as Props. However, there is no
reason that the top level component can't just be a wrapper that then passes
the content of those props into a child component which implements slots.

## Advanced Stuff

### Bundlers
Bundlers allow you to use the full power of Vue. However, they are much trickier
to implement with Drupal. Below are two examples that run the development in a
seperate index.html outside of Drupal but then allow the finished bundle to be
used inside of a block.

#### Vite
[Vite](https://vitejs.dev/) is the latest bundler for Vue 3, but it assumes you
are creating a pure javascript application. Thus, there is a lot of extra work
to jump through to make its final bundle work with Drupal.

The `vue3_vite` block is an example of using Vite to create the final block's
code. Pay special attention to the `vite.config.ts` file which removes file
hashing and uses the external global Vue package provided by Drupal.

Be sure to set the `base` config to the location of your pdb_vue component.

```yaml
# Set your own path so assets will resolve correctly.
base: '/modules/contrib/pdb_vue/components/vue3_vite/dist/',
```

Vite builds bundles as
[ES Module](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
packages so be sure to add the js file as a type `module`.

```js
add_js:
  footer:
    'dist/index.js': { attributes: { type: 'module'} }
```

#### Vue CLI and Webpack
The `vue_example_webpack` (vue 2) block is an example of using the
[Vue CLI](https://github.com/vuejs/vue-cli) to generate single-file components
using ES6 with [Webpack](https://webpack.github.io/).

Be sure to tell webpack that you are using an external version of Vue.js so that
it doesn't load in a second version.
```js
module.exports.externals = {
  "vue": "Vue"
}
```

### State Management

Sharing information between components and block instances is a common need.
This can be done simply with a Global Event Bus or with more advanced tools like
Pinia.

#### Global Event Bus

You may only need to share simple data from instance to instance, in that case,
you can use a Global Event Bus.

- https://codepen.io/segovia94/pen/gOerNjK (Vue 3 - Reactivity API)
- https://codepen.io/segovia94/pen/bGwzPzo (Vue 3 - Mitt library)
- https://codepen.io/segovia94/pen/OJymQxy (Vue 2)

#### Pinia
For more robust state management, the [Pinia](https://pinia.vuejs.org) library
is included. Create your own Store file and be sure to add the
`pdb_vue/vue3.pinia` library as adependency of any blocks.

See the `vue3_pinia_a` and `vue3_pinia_b` example components to see how to use
Pinia with multiple Vue 3 instances.

https://codepen.io/segovia94/pen/jOzqxvV (Vue 3)

You can include the Pinia library by adding a "libraries" parameter to the
`*.info.yml` file like in a theme. This will create a dependency on this
library.

```yaml
libraries:
  - pdb_vue/vue3.pinia
```

#### Vuex
Although Pinia is the recommended state management library for Vue 3, the
[Vuex](https://vuex.vuejs.org) library is included for legacy projects and for
use with Vue 2.

## Override javascript libraries

It is possible that you will need a different version of vue.js or that you will
want to modify a file like the global `vue3.spa-init.js` file loaded by the
`pdb_vue` module. Overriding a library is easy in a theme and can be done by
adding some lines to a custom theme's `themename.info.yml` file.

```yaml
# Replace an entire library.
libraries-override:
  pdb_vue/vue3: themename/vue3
```
or overriding a single file in a library
```yaml
libraries-override:
  vue3.spa-init:
    js:
      js/vue3.spa-init.js: js/custom.spa-init.js
```
Want to completely remove a library because you are managing it with a package manager like Vite?
```yaml
# Remove entire libraries.
libraries-override:
  pdb_vue/vue: false
  pdb_vue/vue.spa-init: false
```
